use std::io::Read;

use sequoia_openpgp as openpgp;

use openpgp::Cert;
use openpgp::Fingerprint;
use openpgp::KeyID;
use openpgp::Result;
use openpgp::crypto::Password;
use openpgp::crypto::mpi;
use openpgp::crypto;
use openpgp::packet::Key;
use openpgp::packet::Packet;
use openpgp::packet::key::PublicParts;
use openpgp::packet::key::UnspecifiedRole;
use openpgp::packet::signature::SignatureBuilder;
use openpgp::parse::PacketParser;
use openpgp::parse::PacketParserResult;
use openpgp::parse::Parse;
use openpgp::parse::stream::GoodChecksum;
use openpgp::parse::stream::MessageLayer;
use openpgp::parse::stream::MessageStructure;
use openpgp::parse::stream::VerificationHelper;
use openpgp::parse::stream::VerifierBuilder;
use openpgp::serialize::stream::LiteralWriter;
use openpgp::serialize::stream::Message;
use openpgp::serialize::stream::Signer;
use openpgp::types::HashAlgorithm;
use openpgp::types::SignatureType;

use anyhow::Context;

/// Test data.
pub mod data;

use crate as backend;

use crate::Backend;
use crate::utils::run_async;

// A wrapper for KeyHandle, which implements crypto::Signer.
pub struct TestSigner(
    Box<dyn crate::KeyHandle + Send + Sync>,
    Key<PublicParts, UnspecifiedRole>);

impl From<Box<dyn backend::KeyHandle + Send + Sync>> for TestSigner {
    fn from(kh: Box<dyn backend::KeyHandle + Send + Sync>) -> Self {
        let key = run_async(async {
            kh.public_key().await
        }).expect("ok"); // XXX: don't unwrap
        Self(kh, key)
    }
}

impl crypto::Signer for TestSigner {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole>
    {
        &self.1
    }

    fn sign(&mut self, hash_algo: HashAlgorithm, digest: &[u8])
            -> Result<mpi::Signature>
    {
        run_async(async {
            self.0.sign(hash_algo, digest).await.map(|(_pk_algo, sig)| sig)
        })?
    }
}

pub struct VerifyHelper {
    // The certificates.
    pub keyring: Vec<Cert>,
    // The good signatures.
    pub signers: Vec<Fingerprint>,
}

impl VerifyHelper {
    pub fn new(keyring: &[ Cert ]) -> Self {
        Self {
            keyring: keyring.to_vec(),
            signers: Vec::new(),
        }
    }
}

impl VerificationHelper for &mut VerifyHelper {
    fn get_certs(&mut self, ids: &[openpgp::KeyHandle]) -> Result<Vec<Cert>> {
        Ok(self.keyring
           .iter()
           .filter(|cert| {
               cert.keys().any(|k| {
                   ids.iter().any(|id| {
                       id.aliases(openpgp::KeyHandle::from(k.fingerprint()))
                   })
               })
           })
           .cloned()
           .collect::<Vec<_>>())
    }
    fn check(&mut self, structure: MessageStructure) -> Result<()> {
        for (i, layer) in structure.into_iter().enumerate() {
            match layer {
                MessageLayer::Encryption { .. } if i == 0 => (),
                MessageLayer::Compression { .. } if i == 1 => (),
                MessageLayer::SignatureGroup { ref results } => {
                    for r in results {
                        if let Ok(GoodChecksum { ka, .. }) = r {
                            self.signers.push(ka.fingerprint());
                        } else {
                            log::info!("Bad signature: {:?}", r);
                        }
                    }
                }
                _ => return Err(anyhow::anyhow!(
                    "Unexpected message structure")),
            }
        }

        Ok(())
    }
}

/// Sign a message and verify that the signature is good.
///
/// If signing_keys is None, this is expected to fail.
pub fn sign_verify(signers: Vec<TestSigner>,
                   keyring: Vec<Cert>,
                   signing_keys: Option<&[ Fingerprint ]>) -> Result<()>
{
    let p = &openpgp::policy::StandardPolicy::new();

    let mut output = Vec::new();
    let message = Message::new(&mut output);

    let builder = SignatureBuilder::new(SignatureType::Binary);

    let mut signers = signers.into_iter();
    let mut signer = Signer::with_template(
        message, signers.next().expect("a signer"), builder);
    for s in signers {
        signer = signer.add_signer(s);
    }
    let signer = signer.build().context("Failed to create signer")?;
    let mut writer = LiteralWriter::new(signer).build()
        .context("Failed to create literal writer")?;

    std::io::copy(&mut std::io::Cursor::new(b"hello"), &mut writer)
        .context("Failed to sign")?;
    if let Err(err) = writer.finalize().context("Failed to sign") {
        if signing_keys.is_none() {
            // Expected failure.
            return Ok(());
        } else {
            panic!("Failed to finalize writer: {}", err);
        }
    }

    let mut h = VerifyHelper::new(&keyring);
    let mut v = VerifierBuilder::from_bytes(&output[..])?
        .with_policy(p, None, &mut h)?;

    let mut message = Vec::new();
    let r = v.read_to_end(&mut message);

    if let Some(signing_keys) = signing_keys {
        assert!(r.is_ok());
        assert_eq!(message, b"hello");
        assert_eq!(&h.signers, signing_keys);
    } else {
        assert!(r.is_err());
    }

    Ok(())
}

/// Try to decrypt a message.
///
/// `ciphertext` is the well-formed encrypted message.
/// plaintext is the expected plaintext.  If it is not `None`,
/// then it must match what is decrypted.
///
/// `password` is an optional password.  If supplied, then it
/// must be used to unlock the key.
///
/// This function tries to decrypt all of the keys with the password.
pub async fn try_decrypt<B>(backend: &mut B,
                            ciphertext: &[u8], plaintext: Option<&[u8]>,
                            keyid: &KeyID, password: Option<Password>)
    -> Result<()>
where B: Backend
{
    let mut good = false;

    let mut session_key = None;
    let mut ppr = PacketParser::from_bytes(ciphertext)?;
    while let PacketParserResult::Some(mut pp) = ppr {
        match pp.packet {
            Packet::PKESK(ref pkesk) => {
                // Manual search.
                'done: for device in backend.list().await {
                    for mut key in device.list().await {
                        if pkesk.recipient() != keyid {
                            continue;
                        }

                        if let Some(password) = password.as_ref() {
                            // Relock, just in case.
                            key.lock().await?;

                            if let Err(e) = key.unlock(password).await {
                                eprintln!("Failed to unlock key: {}", e);
                                // There may be another key that we
                                // can unlock with this password.
                                continue;
                            }
                        }

                        if let Some((algo, sk))
                            = key.decrypt_pkesk(&pkesk).await
                        {
                            session_key = Some((algo, sk));
                            break 'done;
                        }
                    }
                    if session_key.is_none() {
                        return Err(anyhow::anyhow!(
                            "Decryption failed".to_string()));
                    }
                }
            }
            Packet::SEIP(_) => {
                let (algo, sk) = session_key.take().expect("No session key.");
                pp.decrypt(algo, &sk)?;
            }
            Packet::Literal(_) => {
                pp.buffer_unread_content()?;
                if let Packet::Literal(l) = &pp.packet {
                    if let Some(plaintext) = plaintext {
                        if plaintext != l.body() {
                            return Err(anyhow::anyhow!(format!(
                                "Plaintext does not match:\n\
                                 got:      {:?},\n\
                                 expected: {:?}",
                                l.body(), plaintext)));
                        }
                    }
                    good = true;
                }
            }
            _ => (),
        }

        let (_packet, next_ppr) = pp.recurse()?;
        ppr = next_ppr;
    }

    if let PacketParserResult::EOF(ppr) = ppr {
        assert!(ppr.is_message().is_ok());
        // A possible reason this may fail is if the message uses
        // compression, but compression support is not enabled.
        assert!(good);
    }

    Ok(())
}

#[macro_export]
macro_rules! generate_tests {
    ($backend: ty, $init_backend: expr, $import_cert: expr) => {
        #[tokio::test]
        pub async fn decrypt() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            // A simple test: given a secret key and a message encrypted to
            // the key, can we use the backend to decrypt it?

            let mut backend = $init_backend().await;
            let cert = Cert::from_bytes(
                $crate::test_framework::data::key("no-password.asc"))
                .expect("valid cert");

            $import_cert(&mut backend, &cert).await;

            let msg: &[u8] = $crate::test_framework::data::message("no-password.asc");
            let keyid: sequoia_openpgp::KeyID
                = "4EEECCE4F09D9C92".parse().expect("valid");

            $crate::test_framework::try_decrypt(
                &mut backend, msg, Some(b"foo\n"), &keyid, None).await.unwrap();

            Ok(())
        }

        #[tokio::test]
        pub async fn decrypt_missing_password() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            // Load a few variants of a certificate: one where the
            // keys are encrypted with the password "bob", one where
            // they are encrypted with the password "smithy", and one
            // that is not encrypted.

            let mut backend = $init_backend().await;

            let cert = Cert::from_bytes(
                $crate::test_framework::data::key("bob-password-bob.asc"))
                .expect("valid cert");
            $import_cert(&mut backend, &cert).await;

            let cert = Cert::from_bytes(
                $crate::test_framework::data::key("bob-password-smithy.asc"))
                .expect("valid cert");
            $import_cert(&mut backend, &cert).await;

            let msg: &[u8] = $crate::test_framework::data::message("bob.asc");
            let keyid: sequoia_openpgp::KeyID
                = "0A4BA2249168D779".parse().expect("valid");

            assert!(
                $crate::test_framework::try_decrypt(
                    &mut backend, msg, Some(b"hi bob\n"), &keyid, None)
                    .await.is_err());

            Ok(())
        }

        #[tokio::test]
        pub async fn decrypt_with_password() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            // Load a certificate where the keys are encrypted with
            // the password "bob".  Make sure we can decrypt the
            // message.  Load a variant of the certificate where the
            // passwords have been changed.  Make sure we can decrypt
            // the message with the new password.  Finally, load a
            // variant of the certificate where the keys are not
            // encrypted.

            let mut backend = $init_backend().await;

            let msg: &[u8] = $crate::test_framework::data::message("bob.asc");
            let keyid: sequoia_openpgp::KeyID
                = "0A4BA2249168D779".parse().expect("valid");

            // Import Bob's certificate.  They keys are password
            // protected.  Make sure we can use them with the right
            // password, and can't with the wrong one.
            let cert = Cert::from_bytes(
                $crate::test_framework::data::key("bob-password-bob.asc"))
                .expect("valid cert");
            $import_cert(&mut backend, &cert).await;

            assert!(
                $crate::test_framework::try_decrypt(
                    &mut backend, msg, Some(b"hi bob\n"),
                    &keyid, Some(Password::from("sup3r s3cur3")))
                    .await.is_err());
            $crate::test_framework::try_decrypt(
                &mut backend, msg, Some(b"hi bob\n"),
                &keyid, Some(Password::from("bob"))).await.unwrap();

            // Import a new version of Bob's certificate in which the
            // keys are still encrypted, but with a different
            // password.
            let cert2 = Cert::from_bytes(
                $crate::test_framework::data::key("bob-password-smithy.asc"))
                .expect("valid cert");
            assert_eq!(cert.fingerprint(), cert2.fingerprint());
            $import_cert(&mut backend, &cert2).await;

            assert!($crate::test_framework::try_decrypt(
                &mut backend, msg, Some(b"hi bob\n"),
                &keyid, Some(Password::from("sup3r s3cur3"))).await.is_err());
            $crate::test_framework::try_decrypt(
                &mut backend, msg, Some(b"hi bob\n"),
                &keyid, Some(Password::from("smithy"))).await.unwrap();

            // Import a third version of Bob's certificate in which
            // the keys are not encrypt.
            let cert3 = Cert::from_bytes(
                $crate::test_framework::data::key("bob-no-password.asc"))
                .expect("valid cert");
            assert_eq!(cert.fingerprint(), cert3.fingerprint());
            $import_cert(&mut backend, &cert3).await;

            // Note: using the wrong password does not always fail
            // with a key that is not password protected.  For
            // instance, the gpg-agent backend only provides the
            // cached password on demand.  Since the key is not
            // password protected, it won't ask for a password.

            $crate::test_framework::try_decrypt(
                &mut backend, msg, Some(b"hi bob\n"),
                &keyid, None).await.unwrap();

            Ok(())
        }

        #[tokio::test]
        pub async fn verify() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            async fn test(filename: &str, signing_key: &str,
                          password: Option<Password>,
                          success: bool)
                -> Result<()>
            {
                let mut backend = $init_backend().await;

                let cert = Cert::from_bytes(
                    $crate::test_framework::data::key(filename))
                    .expect("valid cert");
                $import_cert(&mut backend, &cert).await;

                let mut signer = backend.find_key(&signing_key).await?;
                if let Some(password) = password.as_ref() {
                    if let Err(err) = signer.unlock(password).await {
                        log::warn!("Failed to unlock key: {}", err);
                    }
                }

                let signing_fpr = Fingerprint::from_hex(signing_key)
                    .expect("valid");

                let expected_signers_;
                let expected_signers: Option<&[ Fingerprint ]> = if success {
                    expected_signers_ = vec![ signing_fpr ];
                    Some(&expected_signers_[..])
                } else {
                    None
                };

                $crate::test_framework::sign_verify(
                    vec![ signer.into() ],
                    vec![ cert.clone() ],
                    expected_signers)?;

                Ok(())
            }

            // A simple test: sign a message using a key with no password.
            test("no-password.asc", "88647F753426C4F16D1ED1E50EF5A4511075ACD7",
                 None, true).await.expect("should pass");

            // Sign a message using a key that is password protected,
            // but don't provide the password.  Make sure it fails.
            test("bob-password-bob.asc", "9410E96E68643821794608269CB5006116833EE7",
                 None, false).await.expect("should pass");

            // Now provide the password.
            test("bob-password-bob.asc", "9410E96E68643821794608269CB5006116833EE7",
                 Some("bob".into()), true).await.expect("should pass");

            // And now provide the wrong password.
            test("bob-password-bob.asc", "9410E96E68643821794608269CB5006116833EE7",
                 Some("wrong password".into()), false).await.expect("should pass");

            Ok(())
        }

        #[tokio::test]
        pub async fn list_keys() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            // We have two variants of a certificate with disjoint
            // subkeys.  When we load both certificates, make sure both
            // sets of variants appear.

            let keyring_a: &[u8] = $crate::test_framework::data::key(
                "carol-subkeys-a.asc");
            let keys_a = &[
                "B0B75BE1004E71FC9FEEF319CC160DCFDE911D68",
                "4BA88DAF3998EF1A1A4DBC9349F15C0BB3DD62B5",
            ];

            let keyring_b: &[u8] = $crate::test_framework::data::key(
                "carol-subkeys-b.asc");
            let keys_b = &[
                "B0B75BE1004E71FC9FEEF319CC160DCFDE911D68",
                "61D8031424FC2C58BBA6CA7F11B1E2EDC6E4D1D1",
                "5606791C6C58CD2A8F1306397189B15EF88BB1D5",
            ];

            // Try with just a, then just b, then both variants.
            for (a, b) in [(true, false), (false, true), (true, true)].iter() {
                let mut keyring: Vec<&[u8]> = Vec::new();
                let mut keys = Vec::new();

                if *a {
                    keyring.push(keyring_a);
                    keys.extend_from_slice(keys_a);
                }
                if *b {
                    keyring.push(keyring_b);
                    keys.extend_from_slice(keys_b);
                }

                let mut backend = init_backend().await;

                for cert_bytes in keyring {
                    let cert = Cert::from_bytes(cert_bytes)
                        .expect("valid cert");

                    $import_cert(&mut backend, &cert).await;
                }

                // Check that we can find the cert by iterating.
                let mut got = Vec::new();
                for device in backend.list().await {
                    for mut key in device.list().await {
                        got.push(key.fingerprint().to_string());
                    }
                }
                got.sort();

                keys.sort();
                keys.dedup();

                assert_eq!(
                    keys.into_iter().map(String::from).collect::<Vec<String>>(),
                    got);

                // Check that Backend::find_key returns all of the keys.
                for (i, k) in keys_a.iter().enumerate() {
                    if *a || i == 0 {
                        assert!(backend.find_key(k).await.is_ok());
                    } else {
                        assert!(backend.find_key(k).await.is_err());
                    }
                }
                for (i, k) in keys_b.iter().enumerate() {
                    if *b || i == 0 {
                        assert!(backend.find_key(k).await.is_ok());
                    } else {
                        assert!(backend.find_key(k).await.is_err());
                    }
                }
            }

            Ok(())
        }

        #[tokio::test]
        pub async fn list_only_returns_secret_keys() -> Result<()> {
            let _ = env_logger::Builder::from_default_env().try_init();

            // Make sure only keys with secret key material are returned.

            let cert_bytes: &[u8] = test_framework::data::key(
                "dave-not-all-keys-have-secrets.asc");
            let cert = "4FD198623B8726286C7159A6032CCEF246B60258";
            let mut keys = vec![
                "AC32461C491647DA6DAC794712CCFC305567FA9D",
            ];
            let public_keys = vec![
                "4FD198623B8726286C7159A6032CCEF246B60258",
                "AA91F4FA667D5B17E8ACCA9F6BC89BD62B55B802",
            ];

            let mut backend = init_backend().await;

            let cert = Cert::from_bytes(cert_bytes)
                .expect("valid cert");
            $import_cert(&mut backend, &cert).await;

            // List the keys.
            let mut got = Vec::new();
            for device in backend.list().await {
                for key in device.list().await {
                    got.push(key.id());
                }
            }
            got.sort();

            keys.sort();
            keys.dedup();

            assert_eq!(
                keys.iter().map(|&k| k.into()).collect::<Vec<String>>(),
                got);

            // Check that Backend::find_key returns all of the keys.
            for k in keys {
                assert!(backend.find_key(k).await.is_ok());
            }
            // And doesn't return the public keys.
            for k in public_keys {
                assert!(backend.find_key(k).await.is_err());
            }

            Ok(())
        }
    };
}
