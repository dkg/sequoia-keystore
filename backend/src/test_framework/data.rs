//! Test data.
//!
//! Includes the test data from `tests/data` in a structured way.
use std::collections::BTreeMap;

/// Returns the content of the given file below `tests/data`.
pub fn file(name: &str) -> &'static [u8] {
    lazy_static::lazy_static! {
        static ref FILES: BTreeMap<&'static str, &'static [u8]> = {
            let mut m: BTreeMap<&'static str, &'static [u8]> =
                Default::default();

            macro_rules! add {
                ( $key: expr, $path: expr ) => {
                    m.insert($key, include_bytes!($path))
                }
            }
            include!(concat!(env!("OUT_DIR"), "/tests.index.rs.inc"));

            // Sanity checks.
            assert!(m.contains_key("keys/no-password.asc"));
            assert!(m.contains_key("messages/no-password.asc"));
            m
        };
    }

    FILES.get(name).unwrap_or_else(|| panic!("No such file {:?}", name))
}

/// Returns the content of the given file below `tests/data/keys`.
pub fn key(name: &str) -> &'static [u8] {
    file(&format!("keys/{}", name))
}

/// Returns the content of the given file below `tests/data/messages`.
pub fn message(name: &str) -> &'static [u8] {
    file(&format!("messages/{}", name))
}
