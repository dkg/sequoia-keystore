A `gpg-agent` backend for Sequoia's private key store.

The `sequoia-keystore` crate implements a server that manages secret
keys.  Secret key material can be stored in files, on hardware devices
like smartcards, or accessed via the network.  `sequoia-keystore`
doesn't implement these access methods.  This is taken care of by
various backends.

This crate includes a backend that exposes the secret keys managed by
a `gpg-agent` process.  By default, this backend uses the default
`gpg-agent`, i.e., the one for `$HOME/.gnupg`.

Whereas the keystore and consequently this backend make use of OpenPGP
data structures, `gpg-agent` uses a lower-level representation, which
is independent of the encoding.  As a first approximation, `gpg-agent`
works with the low-level public and private keys, and does not know
about OpenPGP metadata.  A consequence of this is that it can also
work with X.509 keys.

`gpg-agent` addresses keys using their so-called keygrip, which is
basically a hash of the public key material.  This can usually be
derived from the OpenPGP key material (although the function is not
total).  This backend finds the OpenPGP keys corresponding to the
low-level keys managed by the `gpg-agent` by iterating over all
OpenPGP certificates stored in the user's default [certificate store].
If the backend doesn't find a key with the corresponding keygrip, it
does not expose that key; it is unusable.  If the certificate is
known, the key can be exposed by simply importing the certificate in
the usual way:

```sh
$ sq cert import cert.pgp
```

The backend doesn't need to be restart, it will pick it up on its own.

  [certificate store]: https://gitlab.com/sequoia-pgp/pgp-cert-d

